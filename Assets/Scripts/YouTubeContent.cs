﻿using UnityEngine;
using UnityEngine.UI;

namespace FastYouTubePlayer.Demo
{
    /// <summary>
    /// Demo button for open video gallery
    /// </summary>
    [RequireComponent(typeof(Button))]
    public class YouTubeContent : MonoBehaviour
    {
        #region Fields
        public ContentType contentType;
        public string youTubeURL;
        public Button button;
        #endregion

        #region Methods
        void Start()
        {
            button = button ?? GetComponent<Button>();
            button.onClick.AddListener(() => VideoGalleryManager.getInstance.OpenYouTubeAsync(youTubeURL, contentType).Forget());
        }
        #endregion
    }
}