﻿using UnityEngine;
using UnityEngine.UI;


namespace FastYouTubePlayer
{
    public class GalleryElement : MonoBehaviour
    {
        #region Fields
        [Header("UI")]
        public Image thumbnail;
        public GameObject preloader;
        public Button button;
        [Header("Data")]
        public string thumbnailUrl;
        public string url;
        public int id;
        #endregion

        #region Methods
        void Start()
        {
            button.onClick.AddListener(OpenVideo);
        }

        void OpenVideo()
        {
            VideoGalleryManager.getInstance.CurrentVideoIndex = id;
            MediaPlayerManager.getInstance.OpenYouTubeAsync(url);
        }
        #endregion
    }
}
