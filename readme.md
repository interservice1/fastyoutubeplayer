# Fast YouTube Playlist Player


 Оптимизированный YouTube плеер для interService English Gym 2.0


### Tech
  - [UniTask](https://github.com/Cysharp/UniTask) - Tasks для Unity
  - [YoutubeExplode](https://github.com/Tyrrrz/YoutubeExplode) - Youtube утилиты, для получения прямых ссылок на видео и Thumbnails
  - [AVPro Video](http://renderheads.com/products/avpro-video/) - Плагин для воспроизведения видео


### Features
 - Асинхронность
 - Элементы управления
 - Preloader
 - Thumbnails
 - Выведены основные эвенты
 - Полная поддержка **iOS**, **Android** (ARM7, ARM8)

### Installation

Unity 2020.1.15f

#### 
Использован паттерн **singleton** т.к не указывалось обратное:)

###

