﻿using Cysharp.Threading.Tasks;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace FastYouTubePlayer
{
    public class VideoGalleryManager : Singleton<VideoGalleryManager>
    {
        #region Public Fields 
        [Header("Options")]
        public Transform videoContainer;
        public GameObject videoElementPref;
        public CanvasGroup canvasGroup;
        [Header("Buttons")]
        public Button close;
        #endregion

        #region Private Fields
        private List<GalleryElement> videoElements = new List<GalleryElement>();
        private int _currentVideoIndex = 0;
        #endregion

        #region Props
        public int CurrentVideoIndex
        {
            get { return _currentVideoIndex; }
            set
            {
                _currentVideoIndex = videoElements.Count > value ? value < 0 ? videoElements.Count - 1 : value : 0;
                onVideoIndexChanged.Invoke(_currentVideoIndex);
            }
        }
        #endregion

        #region Events
        public event Action onMetaLoading = () => { };
        public event Action onMetaLoaded = () => { };
        public event Action onThumbnailsLoaded = () => { };
        public event Action<int> onVideoIndexChanged = (_) => { };
        #endregion


        #region Private methods
        void Start()
        {
            close.onClick.AddListener(Hide);
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Open UTube URL video or playlist
        /// </summary>
        /// <param name="url">YouTube video/playlist URL</param>
        /// <param name="contentType">URL type video/playlist</param>
        /// <returns></returns>
        public async UniTaskVoid OpenYouTubeAsync(string url, ContentType contentType)
        {
            Clear();

            onMetaLoading.Invoke();
            var (isTimout, videos) = await YouTubeHelper.GetVideosAsync(url, contentType).TimeoutWithoutException(TimeSpan.FromMilliseconds(5000));
            if (isTimout)
            {
                onMetaLoaded.Invoke();
                return;
            }

            for (int i = 0; i < videos.Count; i++)
            {
                var video = videos[i];
                var ve = Instantiate(videoElementPref, videoContainer).GetComponent<GalleryElement>();
                ve.name = video.Title;
                ve.thumbnailUrl = video.Thumbnails.MediumResUrl;
                ve.url = video.Url;
                ve.id = i;
                videoElements.Add(ve);
            }
            onMetaLoaded.Invoke();
            Show();

            foreach (var video in videoElements)
            {
                var tex = await Networking.GetTextureAsync(video.thumbnailUrl);
                video.thumbnail.sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
                video.preloader.SetActive(false);
            }
            onThumbnailsLoaded.Invoke();
        }

        /// <summary>
        /// Show gallery window 
        /// </summary>
        public void Show()
        {
            canvasGroup.alpha = 1;
            canvasGroup.interactable = true;
            canvasGroup.blocksRaycasts = true;
        }
        /// <summary>
        /// Hide gallery window 
        /// </summary>
        public void Hide()
        {
            canvasGroup.alpha = 0;
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;
        }

        /// <summary>
        /// Get next gallery video 
        /// </summary>
        /// <returns></returns>
        public string GetNext()
        {
            CurrentVideoIndex++;
            return videoElements[CurrentVideoIndex].url;
        }

        /// <summary>
        /// Get previous gallery video 
        /// </summary>
        /// <returns></returns>
        public string GetPrev()
        {
            CurrentVideoIndex--;
            return videoElements[CurrentVideoIndex].url;
        }

        /// <summary>
        /// Clear gallery
        /// </summary>
        public void Clear()
        {
            foreach (Transform element in videoContainer)
            {
                Destroy(element.gameObject);
            }

            videoElements.Clear();
        }
        #endregion
    }
}