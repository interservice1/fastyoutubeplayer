﻿using Cysharp.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using YoutubeExplode;
using YoutubeExplode.Videos.Streams;


namespace FastYouTubePlayer
{
    public enum ContentType : byte
    {
        video,
        playlist
    }

    public static class YouTubeHelper
    {
        #region Fields
        private static YoutubeClient _youtube = new YoutubeClient();
        #endregion


        #region Methods
        public static async UniTask<IReadOnlyList<YoutubeExplode.Videos.Video>> GetVideosAsync(string url, ContentType contentType)
        {
            switch (contentType)
            {
                case ContentType.video:
                    return new List<YoutubeExplode.Videos.Video>() { await GetVideoAsync(url) }.AsReadOnly();
                  
                case ContentType.playlist:
                    return await GetPlalistAsync(url);
                    
                default:
                    break;
            }
            return null;
        }

        public static async Task<IReadOnlyList<YoutubeExplode.Videos.Video>> GetPlalistAsync(string url)
        {
            return await _youtube.Playlists.GetVideosAsync(url);
        }

        public static async Task<YoutubeExplode.Videos.Video> GetVideoAsync(string url)
        {
            return await _youtube.Videos.GetAsync(url);
        }

        public static async Task<(bool error, string result)> GetStreamURLAsync(string videUrl, CancellationToken ct)
        {
            try
            {
                var (isTimout, result) = await GetManifestAsync(videUrl).TimeoutWithoutException(TimeSpan.FromMilliseconds(20000)).WithCancellation(ct);

                return (isTimout || result == null, result.GetMuxed().WithHighestVideoQuality().Url);
            }
            catch
            {
                return (true, string.Empty);
            }
        }

        public static async UniTask<YoutubeExplode.Videos.Streams.StreamManifest> GetManifestAsync(string videUrl)
        {
            return await _youtube.Videos.Streams.GetManifestAsync(videUrl);
        }
        #endregion
    }
}