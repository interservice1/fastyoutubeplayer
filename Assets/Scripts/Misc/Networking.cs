﻿using Cysharp.Threading.Tasks;
using System;
using UnityEngine;
using UnityEngine.Networking;

public enum NetworkStatus:byte
{
    noInternet,
    timeout,
    withInternet,
    tooFrequentRequests
}

public static class Networking
{
    #region Fields
    private static long lastInternetCheck = 0;
    private static long internetCheckDelayInMs = 2000;
    #endregion

    #region Methods
    public static async UniTask<NetworkStatus> IsInternetConnectedInstantAsync()
    {
        if(Application.internetReachability == NetworkReachability.NotReachable) 
            return NetworkStatus.noInternet;

        try
        {
            var (isTimout, result) = await GetResponceAsync(UnityWebRequest.Get("https://fast.com/")).TimeoutWithoutException(TimeSpan.FromMilliseconds(3000));

            if (isTimout) return NetworkStatus.timeout;
            return !isTimout && result != null ? NetworkStatus.withInternet : NetworkStatus.noInternet;
        }
        catch
        {
            return NetworkStatus.noInternet;
        }

    }

    public static async UniTask<NetworkStatus> IsInternetConnectedAsync()
    {
        if (DateTimeOffset.Now.ToUnixTimeMilliseconds() < (lastInternetCheck + internetCheckDelayInMs))
            return NetworkStatus.tooFrequentRequests;

        lastInternetCheck = DateTimeOffset.Now.ToUnixTimeMilliseconds();
        return await IsInternetConnectedInstantAsync();
    }


    public static async UniTask<string> GetResponceAsync(UnityWebRequest req)
    {
        var op = await req.SendWebRequest();
        return op.downloadHandler.text;
    }

    public static async UniTask<Texture2D> GetTextureAsync(string resUrl)
    {
        var op = await UnityWebRequestTexture.GetTexture(resUrl).SendWebRequest();
        return ((DownloadHandlerTexture)op.downloadHandler).texture;
    }
    #endregion
}
