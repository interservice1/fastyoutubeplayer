﻿using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T : Component
{
    #region Fields
    private static T instance;
    #endregion

    #region Props
    public static T getInstance
    {
        get { return instance ?? FindObjectOfType<T>(); }
    }
    #endregion


    #region Methods
    protected virtual void Awake() => instance = getInstance;
    #endregion
}
