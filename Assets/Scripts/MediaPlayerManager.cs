﻿using Cysharp.Threading.Tasks;
using RenderHeads.Media.AVProVideo;
using System;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;


namespace FastYouTubePlayer
{
    public class MediaPlayerManager : Singleton<MediaPlayerManager>
    {
        #region Fields
        [Header("Options")]
        [SerializeField]
        private MediaPlayer mediaPlayer;
        [SerializeField]
        private GameObject mediaPlayerDisplay;
        [SerializeField]
        private GameObject preloader;
        [SerializeField]
        private GameObject controls;
        [Header("Controls")]
        [SerializeField]
        private Button showControls;
        [SerializeField]
        private Button pause;
        [SerializeField]
        private Button next;
        [SerializeField]
        private Button prev;
        [SerializeField]
        private Button close;
        [SerializeField]
        private Text progress;
        [SerializeField]
        private Text duration;
        [SerializeField]
        private Slider volume;
        [SerializeField]
        private Slider seeker;
        [Header("Misc")]
        [SerializeField]
        private Sprite playTex;
        [SerializeField]
        private Sprite pauseTex;


        private float cachedCurrentTimeMs = 0;
        private float currentTimeMs = 0;
        private string cachedURL = string.Empty;
        private CancellationTokenSource cts = new CancellationTokenSource();
        private bool isOpened = false;
        private static long lastBufferingCheck = 0;
        private static long bufferingCheckDelayInMs = 500;
        #endregion

        #region Props
        public bool GetInternetIsDown { get; private set; } = false;
        public bool GetIsPlaying { get; private set; } = false;
        #endregion

        #region Events
        public event Action onLoading = () => { };
        public event Action onLoaded = () => { };
        public event Action whileBuffering = () => { };
        public event Action whileBuffered = () => { };
        public event Action onVideoEnd = () => { };
        public event Action onConnectionDown = () => { };
        public event Action onConnectionUp = () => { };
        public event Action onFirstFrameReady = () => { };
        #endregion


        #region Private methods
        private void Start()
        {
            //controls bindings
            close.onClick.AddListener(Hide);
            next.onClick.AddListener(() => { OpenYouTubeAsync(VideoGalleryManager.getInstance.GetNext()); });
            prev.onClick.AddListener(() => { OpenYouTubeAsync(VideoGalleryManager.getInstance.GetPrev()); });
            pause.onClick.AddListener(TogglePlay);
            showControls.onClick.AddListener(ToggleControls);

            //set events handler
            mediaPlayer.Events.AddListener(MediaPlayerEventHandlerAsync);

            //set events
            onLoading += () => preloader.SetActive(true);
            onFirstFrameReady += () => mediaPlayer.Control.SeekFast(cachedCurrentTimeMs);
            onVideoEnd += () => OpenYouTubeAsync(VideoGalleryManager.getInstance.GetNext());
            onConnectionDown += () => ConnectionDown();
            onConnectionUp += () => ConnectionUp();
            whileBuffering += () => preloader.SetActive(true);
            whileBuffered += () => preloader.SetActive(false);

            seeker.onValueChanged.AddListener(SeekChange);
            volume.onValueChanged.AddListener(VolumeChange);
        }

        private void Update()
        {
            CheckConnectionAsync().Forget();

            CheckBuffering();

            CheckIsPlaying();

            if (GetInternetIsDown)
                cachedCurrentTimeMs = mediaPlayer.Control.GetCurrentTimeMs();


            if (!mediaPlayer.Control.IsPlaying())
                return;

            //UI
            seeker.value = GetSeekerPos();

            duration.text = TimeSpan.FromMilliseconds(mediaPlayer.Info.GetDurationMs()).ToString(@"hh\:mm\:ss");
            progress.text = TimeSpan.FromMilliseconds(mediaPlayer.Control.GetCurrentTimeMs()).ToString(@"hh\:mm\:ss");
        }


        /// <summary>
        /// Media player event handler 
        /// </summary>
        /// <param name="mediaPlayer">media player</param>
        /// <param name="mediaPlayerEvent">event type</param>
        /// <param name="error">error code type</param>
        private async void MediaPlayerEventHandlerAsync(MediaPlayer mediaPlayer, MediaPlayerEvent.EventType mediaPlayerEvent, ErrorCode error)
        {
            switch (mediaPlayerEvent)
            {
                case MediaPlayerEvent.EventType.MetaDataReady:
                    break;
                case MediaPlayerEvent.EventType.ReadyToPlay:
                    break;
                case MediaPlayerEvent.EventType.Started:
                    whileBuffered.Invoke();
                    break;
                case MediaPlayerEvent.EventType.FirstFrameReady:
                    onFirstFrameReady.Invoke();
                    whileBuffered.Invoke();
                    break;
                case MediaPlayerEvent.EventType.FinishedPlaying:
                    onVideoEnd.Invoke();
                    break;
                case MediaPlayerEvent.EventType.Closing:
                    break;
                case MediaPlayerEvent.EventType.Error:
                    if (error == ErrorCode.LoadFailed)
                        if (!GetInternetIsDown && await Networking.IsInternetConnectedInstantAsync() != NetworkStatus.withInternet)
                        {
                            GetInternetIsDown = true;
                            onConnectionDown.Invoke();
                        }
                    break;
                case MediaPlayerEvent.EventType.SubtitleChange:
                    break;
                case MediaPlayerEvent.EventType.Stalled:
                    break;
                case MediaPlayerEvent.EventType.Unstalled:
                    break;
                case MediaPlayerEvent.EventType.ResolutionChanged:
                    break;
                case MediaPlayerEvent.EventType.StartedSeeking:
                    break;
                case MediaPlayerEvent.EventType.FinishedSeeking:
                    break;
                case MediaPlayerEvent.EventType.StartedBuffering:
                    whileBuffering.Invoke();
                    break;
                case MediaPlayerEvent.EventType.FinishedBuffering:
                    whileBuffered.Invoke();
                    break;
                case MediaPlayerEvent.EventType.PropertiesChanged:
                    break;
                case MediaPlayerEvent.EventType.PlaylistItemChanged:
                    break;
                case MediaPlayerEvent.EventType.PlaylistFinished:
                    break;
                default:
                    break;
            }
        }


        private void ConnectionDown()
        {

        }

        private void ConnectionUp()
        {
            ReopenVideoURL(cachedURL);
        }


        /// <summary>
        /// Get clamped seeker slider value from media player time 
        /// </summary>
        /// <returns></returns>
        private float GetSeekerPos()
        {
            float time = mediaPlayer.Control.GetCurrentTimeMs();
            float duration = mediaPlayer.Info.GetDurationMs();
            return Mathf.Clamp(time / duration, 0.0f, 1.0f);
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Open UTube video url 
        /// </summary>
        /// <param name="url">YouTube video URL</param>
        public async void OpenYouTubeAsync(string url)
        {
            if (GetInternetIsDown)
                return;

            Stop();
            onLoading.Invoke();
            Show();

            var answer = await YouTubeHelper.GetStreamURLAsync(url, cts.Token);
            if (answer.error)
            {
                if (!isOpened)
                    Hide();

                cts = new CancellationTokenSource();

                onLoaded.Invoke();
                return;
            }
            cachedURL = answer.result;

            HideControls();
            SetVideoURL(cachedURL);
            onLoaded.Invoke();
            isOpened = true;
        }

        public void CheckIsPlaying()
        {
            if (currentTimeMs != mediaPlayer.Control.GetCurrentTimeMs())
            {
                currentTimeMs = mediaPlayer.Control.GetCurrentTimeMs();
                GetIsPlaying = true;
                return;
            }
            GetIsPlaying = false;
        }

        public void CheckBuffering()
        {
            if (DateTimeOffset.Now.ToUnixTimeMilliseconds() < (lastBufferingCheck + bufferingCheckDelayInMs))
                return;

            lastBufferingCheck = DateTimeOffset.Now.ToUnixTimeMilliseconds();
            if (mediaPlayer.Control.IsBuffering())
            {
                if (mediaPlayer.Control.GetBufferingProgress() < 1 || (!GetIsPlaying && GetInternetIsDown))
                    whileBuffering.Invoke();
                else
                    whileBuffered.Invoke();
            }
        }

        public async UniTaskVoid CheckConnectionAsync()
        {
            var ich = await Networking.IsInternetConnectedAsync();
            switch (ich)
            {
                case NetworkStatus.noInternet:
                case NetworkStatus.timeout:
                    if (!GetInternetIsDown)
                    {
                        GetInternetIsDown = true;
                        onConnectionDown.Invoke();
                    }
                    break;
                case NetworkStatus.withInternet:
                    if (GetInternetIsDown)
                    {
                        GetInternetIsDown = false;
                        onConnectionUp.Invoke();
                    }
                    break;
                case NetworkStatus.tooFrequentRequests:
                default:
                    break;
            }
        }


        /// <summary>
        /// Set video player source url
        /// </summary>
        /// <param name="url"></param>
        public void SetVideoURL(string url)
        {
            if (string.IsNullOrEmpty(url))
                return;

            ResetSeeking();
            mediaPlayer.OpenVideoFromFile(MediaPlayer.FileLocation.AbsolutePathOrURL, url);
            VolumeChange(volume.value);
            Play();
        }

        /// <summary>
        ///  Set video player source url without reset seeking
        /// </summary>
        /// <param name="url"></param>
        public void ReopenVideoURL(string url)
        {
            if (string.IsNullOrEmpty(url))
                return;

            mediaPlayer.OpenVideoFromFile(MediaPlayer.FileLocation.AbsolutePathOrURL, url);
            VolumeChange(volume.value);
            Play();
        }


        public void Play()
        {
            mediaPlayer.Play();
            pause.image.sprite = pauseTex;
        }

        public void Pause()
        {
            mediaPlayer.Pause();
            pause.image.sprite = playTex;
        }

        public void Stop()
        {
            mediaPlayer.CloseVideo();
        }

        public void TogglePlay()
        {
            if (mediaPlayer.Control.IsPlaying())
            {
                Pause();
            }
            else
            {
                Play();
            }
        }

        public void ToggleControls()
        {
            controls.gameObject.SetActive(!controls.gameObject.activeSelf);
        }

        public void ShowControls()
        {
            controls.gameObject.SetActive(true);
        }

        public void HideControls()
        {
            controls.gameObject.SetActive(false);
        }

        public void ResetSeeking()
        {
            cachedCurrentTimeMs = 0;
            SeekChange(0);
            seeker.value = 0;
        }


        //UI event handlers
        public void VolumeChange(float volume)
        {
            mediaPlayer.Control.SetVolume(volume);
        }

        public void SeekChange(float seek)
        {
            progress.text = TimeSpan.FromMilliseconds(seek * mediaPlayer.Info.GetDurationMs()).ToString(@"hh\:mm\:ss");
        }

        public void BeginSeeking()
        {
            mediaPlayer.Pause();
        }

        public void FinishSeeking()
        {
            cachedCurrentTimeMs = seeker.value * mediaPlayer.Info.GetDurationMs();
            mediaPlayer.Control.SeekFast(cachedCurrentTimeMs);
            Play();
        }


        /// <summary>
        /// Show media player window
        /// </summary>
        public void Show()
        {
            cts = new CancellationTokenSource();
            mediaPlayerDisplay.SetActive(true);
        }

        /// <summary>
        /// Hide media player window
        /// </summary>
        public void Hide()
        {
            isOpened = false;
            HideControls();
            cts.Cancel();
            mediaPlayerDisplay.SetActive(false);
            Stop();
        }
        #endregion
    }
}