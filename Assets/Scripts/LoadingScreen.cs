﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace FastYouTubePlayer.Demo
{
    /// <summary>
    /// Demo loading screen
    /// </summary>
    public class LoadingScreen : MonoBehaviour
    {
        #region Fields
        [Header("Options")]
        [SerializeField]
        private CanvasGroup canvasGroup;
        #endregion

        #region Private methods
        void Start()
        {
            VideoGalleryManager.getInstance.onMetaLoading += Show;
            VideoGalleryManager.getInstance.onMetaLoaded += Hide;
        }
        #endregion

        #region Public methods
        public void Show()
        {
            canvasGroup.alpha = 1;
            canvasGroup.blocksRaycasts = true;
        }

        public void Hide()
        {
            canvasGroup.alpha = 0;
            canvasGroup.blocksRaycasts = false;
        }
        #endregion
    }
}
